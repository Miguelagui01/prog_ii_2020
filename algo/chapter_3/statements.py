a = 3
b: int = 3

a + 3
3 + 2

i += 1
j *= 1

assert a == 1


def f(x):
    return x + 1

del(a)


def infinite_sequence():
    num = 0
    while True:
        yield num
        num += 1

gen = infinite_sequence()
next(gen)


def func():
    # Something bad here, I cannot open the database
    raise IOError


try:
    func()
except IOError as exc:
    raise RuntimeError('Failed to open database') from exc


for val in "string":
    if val == "i":
        break
    print(val)

print("The end")


for val in "string":
    if val == "i":
        continue
    print(val)

print("The end")