import torch
from torchvision import datasets, transforms

model = torch.load('ml/feed_forward/itsabird.pt')

label_map = {0: 0, 2: 1, 1: 2}
data_path = 'ml/data-unversioned/'

cifar10_val = datasets.CIFAR10(
    data_path,
    train=False,
    download=False,
    transform=transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize((0.4915, 0.4823, 0.4468),
                             (0.2470, 0.2435, 0.2616))
    ]),
    target_transform=lambda label: label_map[label] if label in label_map else label
)

birds_aeroplanes_val = [index for index, sample in enumerate(cifar10_val) if sample[1] in {0, 1}]

cifar2_val = torch.utils.data.Subset(cifar10_val, birds_aeroplanes_val)

val_loader = torch.utils.data.DataLoader(cifar2_val,
                                         batch_size=64,
                                         shuffle=False)

correct = 0
total = 0

with torch.no_grad():
    for imgs, labels in val_loader:
        outputs = model(imgs.view(imgs.shape[0], -1))
        _, predicted = torch.max(outputs, dim=1)
        total += labels.shape[0]
        correct += int((predicted == labels).sum())

print("Accuracy: %f" % (correct / total))