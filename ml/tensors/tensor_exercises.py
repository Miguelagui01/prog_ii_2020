import torch
a = torch.tensor([[1, 2]], dtype=torch.int64)
b = torch.tensor([[3], [4]], dtype=torch.int64)

"""
>>> a.shape
>>> b.shape
"""

torch.mul(a, b)

"""
b dimension 0 is broadcasted to 
[[3, 3]
 [4, 4]]
 
a dimension 1 is broadcasted to
[[1, 2]
 [1, 2]]
 
result is
[[3, 6]
 [4, 8]]

torch.mul(a, b).size()
Out[59]: torch.Size([2, 2])

"""
a = torch.tensor([[1, 2]], dtype=torch.int64)
a.sum()

b = torch.tensor([0, 4], dtype=torch.int64)
a < b

# Scalar multiplicaation

a = torch.tensor([[1, 2]], dtype=torch.int64)
a * 3
"""
Out[77]: tensor([[3, 6]])
"""

a = torch.tensor([[1, 2]], dtype=torch.int64)
b = torch.tensor([0, 4], dtype=torch.int64)

a + b
"""
Out[78]: tensor([[1, 6]])
"""

a = torch.randn(2, 1)
b = torch.randn(2, 1)
torch.mul(a, b)

a = torch.randn(2, 1)
b = torch.randn(1, 2)
torch.mul(a, b)


# If both tensors are 1-dimensional, the dot product (scalar) is returned.

a = torch.tensor([1, 2])
b = torch.tensor([3, 4])
torch.matmul(a, b)

"""
Out[65]: tensor(11)
"""

# If both arguments are 2-dimensional, the matrix-matrix product is returned.

a = torch.tensor([[1, 2], [3, 2]])
b = torch.tensor([[3, 4], [3, 4]])
torch.matmul(a, b)
"""
Out[66]: 
tensor([[ 9, 12],
        [15, 20]])
"""

# If the first argument is 1-dimensional and the second argument is 2-dimensional, a 1 is
# prepended to its dimension for the purpose of the matrix multiply. After the matrix multiply,
# the prepended dimension is removed.

a = torch.tensor([1, 2])
b = torch.tensor([[3, 4], [3, 4]])
torch.matmul(a, b)

"""
Out[67]: tensor([ 9, 12])
"""

# If the first argument is 2-dimensional and the second argument is 1-dimensional,
# the matrix-vector product is returned.

torch.matmul(b, a)

"""
Out[68]: tensor([11, 11])
"""

# If both arguments are at least 1-dimensional and at least one argument is N-dimensional
# (where N > 2), then a batched matrix multiply is returned.


a = torch.ones(3, 2, 2)

b = torch.tensor([[[2, 2]], [[3, 3]], [[4, 4]]], dtype=torch.float32)

torch.matmul(b, a)

"""
Out[76]: 
tensor([[[4., 4.]],
        [[6., 6.]],
        [[8., 8.]]])
"""

# Storage: shape, offset and stride

points = torch.tensor([[4.0, 1.0], [5.0, 3.0], [2.0, 1.0]])
points.size()
"""
Out[92]: torch.Size([3, 2])
"""

second_point = points[1]
second_point.storage_offset()
"""
Out[93]: 2
"""

second_point.shape
"""
Out[113]: torch.Size([3])
"""


points = torch.tensor([[4.0, 1.0, 5.0], [5.0, 3.0, 6.0], [2.0, 1.0, 7.0]])
points.size()
"""
Out[92]: torch.Size([3, 3])
"""

point_view = points[2]
point_view.storage_offset()
"""
Out[93]: 6
"""

point_view.size()
"""
Out[114]: torch.Size([3])
"""

# In[30]:
points = torch.tensor([[4.0, 1.0], [5.0, 3.0], [2.0, 1.0]])
points_t = points.t()
