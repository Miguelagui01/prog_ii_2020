from concurrent.futures import ProcessPoolExecutor
import time


def process_value(value):
    # print(f'{value} was processed...')
    r = value ** 2
    # print(f'{value} was processed...')
    return r


if __name__ == '__main__':
    t1 = time.perf_counter()
    with ProcessPoolExecutor(max_workers=4) as executor:
        processed_list = executor.map(process_value, range(1000), chunksize=100)
    t2 = time.perf_counter()

    print(list(processed_list))
    print(f'Finished in {t2-t1} seconds')