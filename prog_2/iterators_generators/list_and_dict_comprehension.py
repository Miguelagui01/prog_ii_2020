squares = [x ** 2 for x in range(10)]

different = [(x, y) for x in [1, 2, 3] for y in [3, 1, 4] if x != y]

ascii = {k: chr(k) for k in range(97, 100)}
