from threading import Thread
from time import sleep
import yappi


def worker(msg):
    for i in range(0, 5):
        print(msg, end='', flush=True)
        sleep(1)


yappi.start()

print('Starting')
t1 = Thread(target=worker, args='a', name='t1')
t2 = Thread(target=worker, args='B', name='t2')
t3 = Thread(target=worker, args='C', name='t3')
t1.start()
t1.join()
t2.start()
t3.start()
sleep(3)
print('Done')

yappi.stop()

# retrieve thread stats by their thread id (given by yappi)
threads = yappi.get_thread_stats()
for thread in threads:
    print(
        "Function stats for (%s) (%d)" % (thread.name, thread.id)
    )  # it is the Thread.__class__.__name__
    yappi.get_func_stats(ctx_id=thread.id).print_all()