import pandas as pd
import numpy as np


s = pd.Series(list(range(5)), index=["a", "b", "c", "d", "e"])
s = pd.Series(np.random.randn(5), index=["a", "b", "c", "d", "e"])


s[0] # Basic indexing
s[:3] # Slicing
s[s > s.median()] # Boolean indexing
s[[4, 3, 1]] # Integer indexing
np.exp(s) # ufunc, elementwise operations


s["a"]
s["e"] = 12.0
"e" in s


s + s
# a   -0.264755
# b   -2.464874
# c   -1.670531
# d   -0.247755
# e   -0.959178
# dtype: float64

s[1:] + s[:-1]

# a         NaN
# b   -2.464874
# c   -1.670531
# d   -0.247755
# e         NaN
# dtype: float64
