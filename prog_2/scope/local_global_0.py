"""
This is OK, although c in add shadows name from main scope.
"""

c = 1  # global variable


def add():
    c = 3
    c = c + 2  # increment c by 2
    print(c)


add()
print(c)
