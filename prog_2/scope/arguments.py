def increment(x):
    print(f"Initial address of x: {id(x)}")
    x += 1
    print(f"  Final address of x: {id(x)}")
    print(f"Value of x: {x}")
    return x


def main():
    n = 9001
    print(f"Initial address of n: {id(n)}")
    n = increment(n)
    print(f"  Final address of n: {id(n)}")
    print(f"Value of n: {n}")


main()
