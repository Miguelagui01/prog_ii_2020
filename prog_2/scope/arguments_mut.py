from typing import List


def increment(x: List[int]) -> List:
    """
    Adds 1 to a list

    :param x:   a list
    :return:    a extended list
    """
    print(f"Initial address of x: {id(x)}")
    x.append(1)
    print(f"  Final address of x: {id(x)}")
    print(f"Value of x: {x}")
    return x


def main():
    n = ['a']
    print(f"Initial address of n: {id(n)}")
    n = increment(n)
    print(f"  Final address of n: {id(n)}")
    print(f"Value of n: {n}")


main()
