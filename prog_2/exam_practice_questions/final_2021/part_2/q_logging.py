from typing import Callable

from collections import OrderedDict
import time
import logging

# Empty basic config turns off default console handler

FORMAT = '%(asctime)s [%(levelname)s] %(funcName)s: %(message)s'
logging.basicConfig(format=FORMAT)
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class TimeBoundedLRU:

    def __init__(self, func: Callable, maxsize: int = 128, maxage: int = 30):
        self.cache = OrderedDict()  # { args : (timestamp, result)}
        self.func = func
        self.maxsize = maxsize
        self.maxage = maxage

    def __call__(self, *args):
        if args in self.cache:
            self.cache.move_to_end(args)
            timestamp, result = self.cache[args]
            if time.time() - timestamp <= self.maxage:
                return result
        try:
            result = self.func(*args)
        except TypeError:
            logger.error('Bad Type')
        else:
            self.cache[args] = time.time(), result
            if len(self.cache) > self.maxsize:
                self.cache.popitem(False)
            return result


def query(x: int, y: int) -> int:
    print('Computing...')
    if not (isinstance(x, int) and isinstance(y, int)):
        raise TypeError
    else:
        return x + y


tb_query = TimeBoundedLRU(query)
print(tb_query(1, 3))
print(tb_query('a', 'b'))
