import re
PATTERN = re.compile(r'[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+')

PATTERN_USER = re.compile(r'([a-zA-Z0-9_.+-]+)@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+')


class User:
    def __init__(self, name: str):
        self.name = name
        self._email = None

    @property
    def email(self) -> str:
        return self._email

    @email.setter
    def email(self, new_email: str):
        if re.fullmatch(PATTERN, new_email):
            self._email = new_email

    @property
    def user_name(self) -> str:
        un = re.match(PATTERN_USER, self._email)
        return un.group(1)


u = User('Rob')
u.email = 'roberto.vazquez@ufv.es'
print(u.email)

print(u.user_name)

u = User('Mary')
u.email = 'Mary.Doe'
print(u.email)
