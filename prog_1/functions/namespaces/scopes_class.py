from typing import Final

A_CONSTANT: Final = 3

n = 0

def printing():
    n = 0
    def calcualtion(x) -> int:
        global n
        n += 1
        y = x ** A_CONSTANT
        return y

    def calcualtion_2(x) -> int:
        nonlocal n
        n += 1
        y = x ** 2
        return y

    z = calcualtion(3)
    w = calcualtion_2(5)
    print(z, w)
    print(n)

printing()