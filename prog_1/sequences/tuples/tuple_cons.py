t = 12345, 54321, 'hello!'
t[0]

t

# Tuples may be nested:
u = t, (1, 2, 3, 4, 5)
u

# Tuples are immutable:
t[0] = 88888


# but they can contain mutable objects:
v = ([1, 2, 3], [3, 2, 1])
v


tuple([1,2,3])

tuple(range(0, 10))



# Packing

t = 12345, 54321, 'hello!'

# Unpacking

x, y, z = t

tuple([1, 2, 3])


def fib(n):
    a, b = 0, 1
    for _ in range(n + 1):
        yield a
        a, b = b, a + b


tuple(fib(10))