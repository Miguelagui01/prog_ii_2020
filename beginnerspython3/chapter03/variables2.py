# Variables can hold different types of value

my_variable = 'Jason'
print(my_variable)

"""
Dynamic typing: Type of the data held by a variable 
can dynamically change as the program executes. 
"""

my_variable = 422
print(my_variable)
my_variable = True
print(my_variable)
