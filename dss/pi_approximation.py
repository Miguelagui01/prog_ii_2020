import numpy as np
from math import pi
import itertools
import seaborn

N = 1000

# generate N pseudorandom independent x and y-values on interval [0,1)
x_array = np.random.uniform(low=0, high=1, size=N)
y_array = np.random.uniform(low=0, high=1, size=N)

# Number of pts within the quarter circle x^2 + y^2 < 1 centered at the origin with radius r=1.
in_qtr_circle = sum(x_array ** 2 + y_array ** 2 < 1)

# True area of quarter circle is pi/4 and has N_qtr_circle points within it.
# True area of the square is 1 and has N points within it, hence we approximate pi with
pi_approx = 4 * float(in_qtr_circle) / N  # Typical values: 3.13756, 3.15156

print(pi_approx)


# Graph

def in_circle(acc, p):
    return acc + 1 if p[0] ** 2 + p[1] ** 2 < 1 else acc


in_qtr_circle = np.fromiter(itertools.accumulate(zip(x_array, y_array), in_circle, initial=0),
                            dtype=float)

estimation = 4 * in_qtr_circle[1:] / np.array(range(1, N + 1))

g = seaborn.lineplot(data=estimation, lw=1)
g.axhline(y=pi, color='red', linewidth=0.6, alpha=1)
