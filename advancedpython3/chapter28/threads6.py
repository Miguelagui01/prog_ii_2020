import threading
from threading import Thread
from time import sleep


def worker(msg):
    for i in range(0, 5):
        print(msg, end='', flush=True)
        sleep(1)

def worker_daemon(msg):
    for i in range(0, 10):
        print(msg, end='', flush=True)
        sleep(1)


# Start two normal threads and a daemon thread
t1 = Thread(name='worker-1', target=worker, args='A')
t2 = Thread(name='worker-2', target=worker, args='B')
d = Thread(name='daemon', target=worker_daemon, args='C', daemon=True)

t1.start()
t2.start()
d.start()
